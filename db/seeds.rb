# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
users = User.create([
  {
    first_name: 'Larry',
    last_name: 'Ogunjobi',
    img: 'http://www.charlotteobserver.com/sports/college/football/m644zd/picture87717317/ALTERNATES/FREE_320/LarryOgunjobimediadaypose',
    latitude: '35.305987',
    longitude: '-80.733618'
  },
  {
    first_name: 'Chris',
    last_name: 'Duncan',
    img: 'https://scontent.xx.fbcdn.net/v/t1.0-9/14332950_10206968109940577_3386364559760946183_n.jpg?oh=9aa5eb98b5183a5f5c8152d9f8fcbff8&oe=58AEC705',
    latitude: '35.305759',
    longitude: '-80.733918'
  },
  {
    first_name: 'Phil',
    last_name: 'Johnson',
    img: 'https://thumbs.dreamstime.com/x/handsome-mid-adult-man-portrait-20750946.jpg',
    latitude: '35.306394',
    longitude: '-80.732537'
  },
  {
    first_name: 'Tim',
    last_name: 'Reilly',
    img: 'http://previews.123rf.com/images/ia64/ia641108/ia64110800012/10355216-Male-portrait-serious-adult-man-looking-face-Stock-Photo-bald-man-loss.jpg',
    latitude: '35.307123',
    longitude: '-80.732912'
  },
  {
    first_name: 'Jerry',
    last_name: 'Lee',
    img: 'http://menfash.us/wp-content/uploads/2014/05/Wild-hairstyles-for-older-guys.jpg',
    latitude: '35.308268',
    longitude: '-80.733711'
  },
  {
    first_name: 'Shonda',
    last_name: 'Ray',
    img: 'https://d262ilb51hltx0.cloudfront.net/max/580/1*m1jSHWITBTxFnyz0YQCSiQ.jpeg',
    latitude: '35.307635',
    longitude: '-80.733811'
  },
  {
    first_name: 'Earl',
    last_name: 'Simmons',
    img: 'http://futhead.cursecdn.com/static/img/14/players/204846.png',
    latitude: '35.306188',
    longitude: '-80.731059'
  },
  {
    first_name: 'Shawn',
    last_name: 'Carter',
    img: 'http://www.africanglobe.net/wp-content/uploads/2015/11/Robert-F-Smith-2.jpg',
    latitude: '35.305440',
    longitude: '-80.731574'
  }
])
